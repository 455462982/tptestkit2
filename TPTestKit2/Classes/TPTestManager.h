//
//  TPTestManager.h
//  TPTestKit
//
//  Created by tengpan on 2017/11/30.
//  Copyright © 2017年 tengpan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TPTestManager : NSObject

+ (NSString *)version;

@end

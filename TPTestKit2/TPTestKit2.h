//
//  TPTestKit2.h
//  TPTestKit2
//
//  Created by tengpan on 2017/12/8.
//  Copyright © 2017年 tengpan. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for TPTestKit2.
FOUNDATION_EXPORT double TPTestKit2VersionNumber;

//! Project version string for TPTestKit2.
FOUNDATION_EXPORT const unsigned char TPTestKit2VersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TPTestKit2/PublicHeader.h>

#import <TPTestKit/TPTestManager.h>
